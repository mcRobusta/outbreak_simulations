import math
from numpy import exp

class HeatFlow():

    def __init__(self, entire_grid, all_particles):
        self.grid = entire_grid
        self.particle_list = all_particles
        self.grid_x = 0
        self.grid_y = 0
        self.time = 0

    def get_timestamp(self, timestamp):
        self.time = timestamp

    def compute_grid_coordinates(self, grid_square_id):
        self.grid_x = grid_square_id % 5
        self.grid_y = math.floor(grid_square_id / 5)
        return self.grid_x, self.grid_y

    def compute_grid_id(self, grid_x, grid_y):
        self.grid_id = math.floor(grid_x) + (math.floor(grid_y) * len(self.grid))
        return self.grid_id

    def compute_laplacian(self):
        # Using Laplacian image filter on heat values 
        # around a grid square.
        try:
            laplacian_result = self.grid[self.time][self.grid_x][self.grid_y] * -4
        except IndexError:
            laplacian_result = 0
        try: 
            laplacian_result += self.grid[self.time][self.grid_x-1][self.grid_y]
        except IndexError:
            pass
        try: 
            laplacian_result += self.grid[self.time][self.grid_x+1][self.grid_y]
        except IndexError:
            pass
        try: 
            laplacian_result += self.grid[self.time][self.grid_x][self.grid_y-1]
        except IndexError:
            pass
        try: 
            laplacian_result += self.grid[self.time][self.grid_x][self.grid_y+1]
        except IndexError:
            pass
        if laplacian_result < 0:
            return 0
        return laplacian_result

    def compute_particle_heat(self):
        heat_total = 0
        for particle in self.particle_list:
            geodesic = (particle[1] - self.grid_x)**2
            geodesic += (particle[2] - self.grid_y)**2
            try:
                heat_total += particle[0]/geodesic
            except ZeroDivisionError:
                heat_total += particle[0]
        return heat_total

    def compute_decay(self, grid_square_length):
        area = grid_square_length**2
        exponent = self.time * -1
        exponent /= 2.38
        decay = area * exp(exponent)
        return decay

    def compute_square_heat(self, laplacian_result, heat_total, decay):
        h = 2.38*laplacian_result
        h *= decay
        h += heat_total
        h /= h+1
        h *= h 
        return h