# outbreak_simulations

Provides code used to simulate pandemic training data and build a functional discriminator.

To run: 
    Clone this repository (https://gitlab.com/mcrobusta/outbreak_simulations.git)
    Install requirements (pip3 install -r modules.txt, Windows users use 'pip' instead of 'pip3')
    Create a Rockset account at console.rockset.com
    Get your API key
    Replace [REDACTED] with API key and [COLLECTION] with the name you want your collection to be called
    From the command line, run:
        (MacOS/Linux):
        python3 simulation_modelling.py 
        (Windows): 
        python simulation_modelling.py
