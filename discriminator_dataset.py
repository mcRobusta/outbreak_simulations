from rockset import Client, Q, F # Q for query, F for field
import json
import numpy
import torch
from torch.utils.data import Dataset, DataLoader

# Design neural network that can predict
# simulation outcomes from 3 snapshots at 
# varying time intervals.

class DiscriminatorDataset(torch.nn.Module):

    def __init__(self, rockset_collection, rockset_key):
        self.rockset_collection = rockset_collection
        self.rockset_key = rockset_key
        # Placed Rockset access code in __init__ to 
        # automatically run on initialisation.
        rs = Client(api_key=self.rockset_key)
        getDataQuery = Q(self.rockset_collection).select(F['Tensor'], F['MetaData'])
        self.results = rs.sql(getDataQuery)

    def __len__(self):
        #return len(self.results)
        return 1000
    
    def process_rockset(self, iterator):
        tensor = self.results[iterator]["Tensor"]
        print(tensor)
        tensor = numpy.array(tensor)
        tensor = torch.from_numpy(tensor, dtype=torch.float16)
        meta = self.results[iterator]["MetaData"]
        return tensor, meta

    def __getitem__(self, index):
        return self.process_rockset(index)