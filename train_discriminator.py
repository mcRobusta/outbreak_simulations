import torch
from torch import optim
import torch.nn as neural_net
from discriminator_dataset import DiscriminatorDataset
from random import randint
import numpy
# Discriminator class inherits standard PyTorch neural net class
# and produces trainable discriminator/decoder pair. Uses standard
# multiple linear regression networks for this task.
#cuda = torch.device('cuda')
class Discriminator(neural_net.Module):
    # Our end goal is to have a pair of neural networks that 
    # compare encoded->decoded data with the original data. 
    # Thus, the input data by itself is our full training dataset.
    def __init__(self, rockset_collection, rockset_key):
        # data_length is the length of the data array of every tensor (a square tensor is assumed).
        super(Discriminator, self).__init__()
        try:
            self.dataset = DiscriminatorDataset(rockset_collection, rockset_key)
        except Exception as exception:
            print(exception)
        self.discriminator = neural_net.Sequential(
            # Note that the input size given to nn.Linear is the side length of an expected square tensor.
            neural_net.Linear(5*5*5 + 1, 3*5*5),
            neural_net.Sigmoid(),
            neural_net.Linear(3*5*5, 5*5),
            neural_net.Sigmoid(),
            neural_net.Linear(5*5, 2*5),
            neural_net.Sigmoid(),
            neural_net.Linear(2*5, 5),
            neural_net.Sigmoid(),
            neural_net.Linear(5, 2),
            neural_net.Sigmoid()
        )

    def loss_function(self):
        return neural_net.CTCLoss() # Initialise Connectionist Temporal Classification loss class (good for continuous time classifications).

    def train_discriminator(self, learning_rate):
        # network_parameters = discriminator(folder, 10, 2).parameters()
        optimiser = optim.SGD(self.discriminator.parameters(), lr=learning_rate, momentum=0.5)
        loss_function = self.loss_function() # Self function is used to allow for effective class override.
        dataset_size = self.dataset.__len__()
        for iterator in range(dataset_size):
            rockset_data = self.dataset.__getitem__(iterator)
            rockset_tensor = rockset_data[0]
            rockset_slices = []
            slice_meta = []
            num_slices = len(rockset_tensor)
            print(len(rockset_tensor))
            for tensor_iterator in range(5):
                rand_index = randint(0, num_slices-1)
                new_slice = rockset_tensor[rand_index]
                new_slice.append(rand_index)
                new_slice = numpy.array(new_slice)
                rockset_slices.append(new_slice)
            rockset_slices = numpy.array(rockset_slices)
            rockset_slices = torch.from_numpy(rockset_slices) #, device=cuda)
            result_predicted = self.discriminator(rockset_slices)
            result_actual = rockset_data[1]
            loss = loss_function(results_predicted, results_actual)
            loss.backward()
            optimiser.step()
            optimiser.zero_grad()
            iterator += 1
            if iterator%100 == 0:
                    print('Training iteration: [' + str(iterator) + ']/[' + dataset_size + ']. Current error: ' + str(loss.data.item()))

    def save_trained_model(self, filepath):
        torch.save(self.discriminator.state_dict(), filepath + '/discriminator.pth')

d = Discriminator([COLLECTION], [REDACTED])
d.train_discriminator(0.05)
discriminator.save_trained_model('')
