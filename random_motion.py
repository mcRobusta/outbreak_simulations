import math
import os
from numpy import exp

class RandomMotion():
    # Get the frequented location and current position.
    # Computes new random position and probability of 
    # viral contraction.

    def __init__(self, frequent_coordinates):
        self.frequent_x = frequent_coordinates[0]
        self.frequent_y = frequent_coordinates[1]
        self.eMinusOne = exp(-1)
        self.eMinusTwo = exp(-2)

    def compute_random(self):
        return int.from_bytes(os.urandom(8), byteorder="big") / ((1 << 64) - 1)

    def get_time_delta(self, time_since_last_freq_visit):
        self.t_delta = time_since_last_freq_visit

    def get_current_coordinates(self, current_coords):
        self.current_x = current_coords[0]
        self.current_y = current_coords[1]

    def compute_new_position(self):
        freq_visit = False
        probability_freq_visit = self.compute_random()*(self.t_delta/(self.t_delta+1))
        if probability_freq_visit >= 0.5:
            self.new_x = self.frequent_x
            self.new_y = self.frequent_y
            freq_visit = True
        else:
            self.new_x = self.compute_random()
            self.new_y = self.compute_random()
        return self.new_x, self.new_y, freq_visit

    def compute_probability_decay(self, last_three_heatsquares):
        last_three_heatsquares[1] *= self.eMinusOne
        last_three_heatsquares[2] *= self.eMinusTwo
        probability_decay = last_three_heatsquares[0]
        probability_decay += last_three_heatsquares[1]
        probability_decay += last_three_heatsquares[2]
        return probability_decay

    def compute_probability_contraction(self, result):
        active_result = math.atan(result)
        active_result /= math.pi
        active_result += 0.5
        if active_result > 0.5:
            return True
        else: 
            return False

    def compute_probability_death(self):
        # Runs 14 times per patient. Every 
        # patient has a 2% chance of death.
        # Thus, running 14 times should yield
        # total C.O.D. of 2%. Thus:
        # p(dying)*14 = 0.02
        # p(!dying) = 1 - (0.02/14)
        #           = 0.09986
        if self.compute_random() > 0.9986:
            return True
        else:
            return False