from rockset import Client, Q, F
import numpy
import random
from random_motion import RandomMotion
from heat_flow import HeatFlow
from scipy.stats import norm
import torch
import math
import os 

def compute_random():
    return int.from_bytes(os.urandom(8), byteorder="big") / ((1 << 64) - 1)

rs = Client(api_key=[REDACTED])

try:
    training = rs.Collection.create([REDACTED])
except: 
    training = rs.Collection.retrieve([REDACTED])

def main():
    grid = [[[0 for i in range(5)] for j in range(5)]]
    particle_list = [] 
    num_people = random.randint(1, 1000)
    for k in range(num_people):
        is_carrier = False
        probability_carrier = compute_random()
        if probability_carrier > 0.8:
            is_carrier = True
        particle_list.append([0, compute_random(), compute_random(), 0, [0,0,0], is_carrier])
    freq_coordinates = [compute_random(), compute_random()]
    r = RandomMotion(freq_coordinates)
    h = HeatFlow(grid, particle_list)

    num_deaths = 0
    num_recovered = 0
    pndmc_length = 0
    pandemicActive = True

    while pandemicActive:
        grid.append([[0 for i in range(5)] for j in range(5)])
        pandemicActive = False
        toDelete = []
        for index, particle in enumerate(particle_list):
            h.get_timestamp(pndmc_length)
            r.get_time_delta(particle[3])
            r.get_current_coordinates([particle[1], particle[2]])
            particle[1], particle[2], isFrequent = r.compute_new_position()
            if isFrequent:
                particle[3] = 0
            else:
                particle[3] += 1
            for grid_iterator in range(25):
                x, y = h.compute_grid_coordinates(grid_iterator)
                laplacian = h.compute_laplacian()
                heat_p = h.compute_particle_heat()
                decay = h.compute_decay(0.2)
                square_heat = h.compute_square_heat(laplacian, heat_p, decay)
                grid[pndmc_length][x][y] = square_heat
            particle[4][2] = particle[4][1]
            particle[4][1] = particle[4][0]
            particle[4][0] = grid[pndmc_length][math.floor(particle[1]*5)][math.floor(particle[2]*5)]
            if particle[5]:
                particle[0] += math.floor(14*(norm.rvs()**2/14))
                pandemicActive = True
                if r.compute_probability_death():
                    num_deaths += 1
                    toDelete.append(index) # 'del particle' only deletes the loop pointer, NOT the array element
            if particle[0] > 14:
                particle[5] = False
                particle[0] = 0
                num_recovered += 1
                toDelete.append(index)
            p_decay = r.compute_probability_decay(particle[4])
            particle[5] = r.compute_probability_contraction(p_decay)
        pndmc_length += 1
        new_indices = [n for n in range(len(particle_list)) if n not in toDelete]
        particle_list = [particle_list[n] for n in new_indices]
        #output_string = "Day " + str(pndmc_length) + "; Number recovered: " + str(num_recovered) + ", Number of Deaths: " + str(num_deaths)
        #print(output_string, end="\n", flush=True)

    rockset_numpy = []

    for snapshot_iterator, snapshot in enumerate(grid):
        colour_list = []
        colour_numpy = [[0 for i in range(5)] for j in range(5)]
        for row in snapshot:
            for item in row:
                colour = item
                colour_list.append(colour)
        for counter, item in enumerate(colour_list):
            index_y = counter % 5
            index_x = math.floor(counter / 5)
            colour_numpy[index_x][index_y] = item
        rockset_numpy.append(colour_numpy)

    #rockset_numpy = numpy.array(rockset_numpy)

    #print(rockset_numpy)
    print("Final Count:\nPandemic Length: " + str(pndmc_length) + " || Number of Deaths: " + str(num_deaths) + " || Death Rate: " + str((num_deaths/num_people)*100)[:3] + r"%", end='\n', flush=True)
    training.add_docs([{"Tensor": rockset_numpy, "MetaData": {"time": pndmc_length, "deathrate": num_deaths/num_people}}])

for x in range(1000):
    main()
    if x % 10 == 0:
        print(str(math.floor(x/10)) + r" % complete.", flush=True)
